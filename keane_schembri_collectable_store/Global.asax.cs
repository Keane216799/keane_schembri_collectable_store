﻿using keane_schembri_collectable_store.Controllers;
using keane_schembri_collectable_store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace keane_schembri_collectable_store
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        protected void Application_Error(object sender, EventArgs e) {
            Exception lastException = Server.GetLastError();
            string actionName = Request.Path;
            Error error = new Error();
            ErrorController ec = new ErrorController();
            error.errorValue = ec.LogError(User.Identity.Name, lastException, "", actionName);
            db.Errors.Add(error);
            db.SaveChanges();
            Server.ClearError();
            Response.Redirect("\\Error.html");
        }
    }
}
