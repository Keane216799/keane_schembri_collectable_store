﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using keane_schembri_collectable_store.Models;

namespace keane_schembri_collectable_store.Controllers
{
    public class QualitiesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Qualities
        public ActionResult Index()
        {
            return View(db.Qualities.ToList());
        }

        // GET: Qualities/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Quality quality = db.Qualities.Find(id);
            if (quality == null)
            {
                return HttpNotFound();
            }
            return View(quality);
        }

        // GET: Qualities/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Qualities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "qualityID,quality")] Quality quality)
        {
            if (ModelState.IsValid)
            {
                db.Qualities.Add(quality);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(quality);
        }

        // GET: Qualities/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Quality quality = db.Qualities.Find(id);
            if (quality == null)
            {
                return HttpNotFound();
            }
            return View(quality);
        }

        // POST: Qualities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "qualityID,quality")] Quality quality)
        {
            if (ModelState.IsValid)
            {
                db.Entry(quality).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(quality);
        }

        // GET: Qualities/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Quality quality = db.Qualities.Find(id);
            if (quality == null)
            {
                return HttpNotFound();
            }
            return View(quality);
        }

        // POST: Qualities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Quality quality = db.Qualities.Find(id);
            db.Qualities.Remove(quality);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
