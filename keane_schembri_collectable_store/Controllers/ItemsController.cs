﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using keane_schembri_collectable_store.Models;
using PagedList;

namespace keane_schembri_collectable_store.Controllers
{
    [Authorize]
    public class ItemsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Items
        public ActionResult Index()
        {
            var items = db.Items.Include(i => i.ItemTypes).Include(i => i.Quality);
            return View(items.ToList());
        }

        // GET: Items/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = db.Items.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        // GET: Items/Create
        public ActionResult Create()
        {
            ViewBag.ItemTypeId = new SelectList(db.ItemTypes, "itemTypeID", "itemTypeName");
            ViewBag.QualityId = new SelectList(db.Qualities, "qualityID", "quality");
            return View();
        }

        // POST: Items/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ItemId,ItemTypeId,SellerId,Quantity,QualityId,Price")] Item item)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = db.Users.FirstOrDefault(x => x.UserName == User.Identity.Name); //logged in user
                    item.Seller = user;
                    DateTime date = DateTime.Now;
                    item.Date = date;
                    db.Items.Add(item);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }catch(DataException)
            {
                ModelState.AddModelError("", "This Item already EXISTS!");
                ViewBag.ItemTypeId = new SelectList(db.ItemTypes, "itemTypeID", "itemTypeName", item.ItemTypeId);
                ViewBag.QualityId = new SelectList(db.Qualities, "qualityID", "quality", item.QualityId);
                return View(item);
            }

            return View(item);

        }

        // GET: Items/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = db.Items.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            var users = db.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            if (item.SellerId != users.Id)
            {
                return RedirectToAction("Index");
            }
            ViewBag.ItemTypeId = new SelectList(db.ItemTypes, "itemTypeID", "itemTypeName", item.ItemTypeId);
            ViewBag.QualityId = new SelectList(db.Qualities, "qualityID", "quality", item.QualityId);
            return View(item);
        }

        // POST: Items/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ItemId,ItemTypeId,SellerId,Quantity,QualityId,Price")] Item item)
        {
            if (ModelState.IsValid)
            {
                var user = db.Users.FirstOrDefault(x => x.UserName == User.Identity.Name); //logged in user
                if(item.SellerId!=user.Id)
                {
                    return RedirectToAction("Index");
                }
                item.Date = DateTime.Now;
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ItemTypeId = new SelectList(db.ItemTypes, "itemTypeID", "itemTypeName", item.ItemTypeId);
            ViewBag.QualityId = new SelectList(db.Qualities, "qualityID", "quality", item.QualityId);
            return View(item);
        }

        // GET: Items/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = db.Items.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            var users = db.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            if (item.SellerId != users.Id) {
                return RedirectToAction("Index");
            }
            return View(item);
        }

        // POST: Items/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Item item = db.Items.Find(id);
            db.Items.Remove(item);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        public ActionResult Search(int? page)
        {

            int size = 5;
            int pageN = (page ?? 1);

            var items = from i in db.Items
                        select i;

            var users = db.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            items = items.OrderByDescending(i => i.Date);
            return View(items.ToList().ToPagedList(pageN, size));

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
