﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Google.Apis.Drive.v3;
using keane_schembri_collectable_store.Models;

namespace keane_schembri_collectable_store.Controllers
{
    [Authorize]
    public class ItemTypesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ItemTypes
        public ActionResult Index()
        {
            var itemTypes = db.ItemTypes.Include(i => i.Category);
            return View(itemTypes.ToList());
        }

        // GET: ItemTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemType itemType = db.ItemTypes.Find(id);
            if (itemType == null)
            {
                return HttpNotFound();
            }
            return View(itemType);
        }

        // GET: ItemTypes/Create
        public ActionResult Create()
        {
            ViewBag.categoryID = new SelectList(db.Categories, "categoryID", "categoryName");
            return View();
        }

        // POST: ItemTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "itemTypeID,itemTypeName,categoryID,image")] ItemType itemType, HttpPostedFileBase file)
        {
            byte[] bytesRead = new byte[2];
            file.InputStream.Read(bytesRead, 0, 2);

            if (ModelState.IsValid)
            {
                if (file == null)
                {
                    ModelState.AddModelError("", "Please Choose an Image");
                }
                else
                {
                    if((bytesRead[0] == 137 && bytesRead[1] == 80) || (bytesRead[0] == 255 && bytesRead[1] == 216)) {
                        file.InputStream.Position = 0;
                        //create service
                        var service = GoogleDriveApi.GetService();
                        var FileMetaData = new Google.Apis.Drive.v3.Data.File();
                        FileMetaData.Name = Path.GetFileName(file.FileName);
                        FileMetaData.MimeType = file.ContentType;
                        FilesResource.CreateMediaUpload request;
                        using (var stream = file.InputStream)
                        {
                            request = service.Files.Create(FileMetaData, stream, FileMetaData.MimeType);
                            request.Fields = "id";
                            request.Upload();
                        }
                        var fileid = request.ResponseBody;
                        itemType.image = "https://drive.google.com/uc?id=" + fileid.Id;
                        var category = db.Categories.FirstOrDefault(x => x.categoryID == itemType.categoryID);
                        itemType.Category = category;
                        db.ItemTypes.Add(itemType);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                    else
                        ModelState.AddModelError("", "Invalid File Uploaded");
                }
    
            }

            ViewBag.categoryID = new SelectList(db.Categories, "categoryID", "categoryName", itemType.categoryID);
            return View(itemType);
        }

        // GET: ItemTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemType itemType = db.ItemTypes.Find(id);
            if (itemType == null)
            {
                return HttpNotFound();
            }
            ViewBag.categoryID = new SelectList(db.Categories, "categoryID", "categoryName", itemType.categoryID);
            return View(itemType);
        }

        // POST: ItemTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "itemTypeID,itemTypeName,categoryID,image")] ItemType itemType, HttpPostedFileBase file)
        {
            byte[] bytesRead = new byte[2];
            file.InputStream.Read(bytesRead, 0, 2);

            if (ModelState.IsValid)
            {
                if (file == null)
                {
                    ModelState.AddModelError("", "Please Choose an Image");
                }
                else
                {
                    if ((bytesRead[0] == 137 && bytesRead[1] == 80) || (bytesRead[0] == 255 && bytesRead[1] == 216))
                    {
                        file.InputStream.Position = 0;
                        //create service
                        var service = GoogleDriveApi.GetService();
                        var FileMetaData = new Google.Apis.Drive.v3.Data.File();
                        FileMetaData.Name = Path.GetFileName(file.FileName);
                        FileMetaData.MimeType = file.ContentType;
                        FilesResource.CreateMediaUpload request;
                        using (var stream = file.InputStream)
                        {
                            request = service.Files.Create(FileMetaData, stream, FileMetaData.MimeType);
                            request.Fields = "id";
                            request.Upload();
                        }
                        var fileid = request.ResponseBody;
                        itemType.image = "https://drive.google.com/uc?id=" + fileid.Id;
                        db.Entry(itemType).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                    else
                        ModelState.AddModelError("", "Invalid File Uploaded");
                }
            }

            ViewBag.categoryID = new SelectList(db.Categories, "categoryID", "categoryName", itemType.categoryID);
            return View(itemType);
        }

        // GET: ItemTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemType itemType = db.ItemTypes.Find(id);
            if (itemType == null)
            {
                return HttpNotFound();
            }
            return View(itemType);
        }

        // POST: ItemTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ItemType itemType = db.ItemTypes.Find(id);
            db.ItemTypes.Remove(itemType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
