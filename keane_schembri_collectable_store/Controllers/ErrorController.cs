﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace keane_schembri_collectable_store.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index()
        {
            return View();
        }

        public string LogError(string username, Exception ex, string page, string method) {
            string errorValue = "";
            if (ex.InnerException != null)
            {
                Trace.WriteLine(errorValue = String.Format("Username {0}, Exception {1}, Message {2}, Page {3}, Method {4}, Inner Exception {5}, Date {6}", username, ex.GetType().ToString(), ex.Message, page, method, ex.InnerException.Message, DateTime.Now));
            }
            else {
                Trace.WriteLine(errorValue = String.Format("Username {0}, Exception {1}, Message {2}, Page {3}, Method {4}, Inner Exception {5}, Date {6}", username, ex.GetType().ToString(), ex.Message, page, method, "null", DateTime.Now));
            }
            return errorValue;
        }
    }
}