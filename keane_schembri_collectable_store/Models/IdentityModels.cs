﻿//using System;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using keane_schembri_collectable_store.Controllers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace keane_schembri_collectable_store.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("CollectableStore", throwIfV1Schema: false)
        {
            Database.SetInitializer(new webApp());
        }

        public class webApp : DropCreateDatabaseIfModelChanges<ApplicationDbContext> {

            public override void InitializeDatabase(ApplicationDbContext context)
            {
                base.InitializeDatabase(context);
            }

            protected override void Seed(ApplicationDbContext context)
            {
                base.Seed(context);

                //Quality
                List<Quality> quality = new List<Quality>();
                quality.Add(new Quality() { quality = "Excellent" });
                quality.Add(new Quality() { quality = "Good" });
                quality.Add(new Quality() { quality = "Poor" });
                quality.Add(new Quality() { quality = "Bad" });
                context.Qualities.AddRange(quality);

/*      Testing Data - uncomment until line 115 :)    
 *      
                //Categories
                List<Category> categories = new List<Category>();
                for (int i = 1; i <= 5; i++)
                {
                    categories.Add(new Category() { categoryName = "test" + i + "category" });
                }
                context.Categories.AddRange(categories);

                //Users
                using (RoleManager<IdentityRole> roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context)))
                {

                    if (!roleManager.RoleExists("User"))
                    {
                        IdentityRole role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                        role.Name = "User";
                        roleManager.Create(role);
                    }
                }

                List<ApplicationUser> users = new List<ApplicationUser>();
                using (UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context)))
                {
                    for (int i = 1; i <= 5; i++)
                    {
                        ApplicationUser seedingUser = new ApplicationUser()
                        {
                            Email = "test" + i + "@hotmail.com",
                            UserName = "test" + i + "@hotmail.com"
                        };
                        context.Users.Add(seedingUser);
                        userManager.Create(seedingUser, "password");
                        userManager.AddToRole(seedingUser.Id, "User");
                    }
                }

                //ItemTypes
                List<ItemType> itemtype = new List<ItemType>();
                for (int i = 1; i <= 5; i++)
                {
                    itemtype.Add(new ItemType() { itemTypeName = "itemTypeName" + i, categoryID = 1 });
                    itemtype.Add(new ItemType() { itemTypeName = "itemTypeName" + i, categoryID = 2 });
                    itemtype.Add(new ItemType() { itemTypeName = "itemTypeName" + i, categoryID = 3 });
                    itemtype.Add(new ItemType() { itemTypeName = "itemTypeName" + i, categoryID = 4 });
                    itemtype.Add(new ItemType() { itemTypeName = "itemTypeName" + i, categoryID = 5 });
                }
                context.ItemTypes.AddRange(itemtype);
                context.SaveChanges();

                //Items
                AccountController ac = new AccountController();
                var rand = new Random();
                List<Item> items = new List<Item>();

                for (int i = 0; i <= 99; i++)
                {
                    items.Add(new Item() { ItemTypeId = (rand.Next(1, 11)), SellerId = ac.GetRandomUserID(),
                        Quantity = (rand.Next(1, 101)), QualityId = (rand.Next(1, 5)),
                        Price = i + 10f, Date = DateTime.Now });
                }
                context.Items.AddRange(items);
 */               
                context.SaveChanges();
            }
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<keane_schembri_collectable_store.Models.Category> Categories { get; set; }

        public System.Data.Entity.DbSet<keane_schembri_collectable_store.Models.Quality> Qualities { get; set; }

        public System.Data.Entity.DbSet<keane_schembri_collectable_store.Models.Item> Items { get; set; }

        public System.Data.Entity.DbSet<keane_schembri_collectable_store.Models.ItemType> ItemTypes { get; set; }

        public System.Data.Entity.DbSet<keane_schembri_collectable_store.Models.Error> Errors { get; set; }
    }
}