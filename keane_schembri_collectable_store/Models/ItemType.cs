﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace keane_schembri_collectable_store.Models
{
    public class ItemType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int itemTypeID { get; set; }

        [Required(ErrorMessage = "Item Type Name is Required")]
        public string itemTypeName { get; set; }

        [Required]
        public int categoryID { get; set; }
        public virtual Category Category { get; set; }

        public string image { get; set; }

        public virtual ICollection<Item> Item{ get; set;} 
    }
}