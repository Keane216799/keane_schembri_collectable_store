﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace keane_schembri_collectable_store.Models
{
    public class Quality
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int qualityID { get; set; }

        [Required]
        public string quality { get; set; }
    }
}